
function clearAuthentication() {
    localStorage.removeItem(AUTH_TOKEN);
    localStorage.removeItem(AUTH_USERNAME);
}

function storeAuthentication(session) {
    localStorage.setItem(AUTH_TOKEN, session.token);
    localStorage.setItem(AUTH_USERNAME, session.username);
    localStorage.setItem(AUTH_LOGO, session.logo);
}

function getUsername() {
    return localStorage.getItem(AUTH_USERNAME);
}

function getToken() {
    return localStorage.getItem(AUTH_TOKEN);
}

//05.12 userObject'ist logo URL topContainer'i jaoks:
function getUserLogo() {
    return localStorage.getItem(AUTH_LOGO);
}