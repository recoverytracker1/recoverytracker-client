function generateTopMenu() {
    let logoutLink = "";
    if (!isEmpty(getUsername())) {
        logoutLink = /*html*/`
            <img src="${getUserLogo()}" height="15">
            ${getUsername()} | <a href="javascript:logoutUser()" style="color: #fff; font-weight: bold;">Log Out</a>
            `;
    }

    document.getElementById("topMenuContainer").innerHTML = /*html*/`
    <div style="padding: 5px; color: white; font-style: 'Roboto' sans-serif; display: flex; justify-content: space-between">        
        <div >Your Online Medical Data</div>
        <div style="text-align: right; font-style: normal;">
            ${logoutLink}
        </div>
    </div>
`;
}

function showLoginContainer() {
    document.getElementById("loginContainer").style.display = "block";
    document.getElementById("mainContainer").style.display = "none";
}

function showMainContainer() {
    document.getElementById("loginContainer").style.display = "none";
    document.getElementById("mainContainer").style.display = "block";
}


//rippmenüüst vaate vahetamine index.html-i?
//if login == true and value === "CHART" then showMainContainer(); else show log_data view/update
function changeView() {
    if(document.getElementById("viewSelect").value === "INSERT") {
        document.querySelector("#myMedDataForms").style.display = "block";
        document.querySelector("#myMedDataLogs").style.display = "none";
        document.querySelector("#myMedDataView").style.display = "none";

    } else if (document.getElementById("viewSelect").value === "LOG") {
        document.querySelector("#myMedDataForms").style.display = "none";
        document.querySelector("#myMedDataLogs").style.display = "block";
        document.querySelector("#myMedDataView").style.display = "none";
        loadLogTable();
    } else { 
        document.querySelector("#myMedDataForms").style.display = "none";
        document.querySelector("#myMedDataLogs").style.display = "none";
        document.querySelector("#myMedDataView").style.display = "block";
        loadLogView();
    }
}


