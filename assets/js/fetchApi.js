// Functions for communicating with backend-API here...

function fetchUser(id) {
    return fetch(
        `${API_URL}/medinfo/${id}`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse).then(user => user.json());
}

//03.12.2019 siia meie logivaade fetchimiseks
function fetchLogView() {
    return fetch(
        `${API_URL}/medinfo/username/${getUsername()}`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
            
        }
    )
    .then(logData => logData.json());
}


function postUser(user) {
    return fetch(
        `${API_URL}/medinfo`, 
        {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(user)
        }
    )
    .then(checkResponse);
}

function postMedData(medData) {
    return fetch(
        `${API_URL}/medinfo`, 
        {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(medData)
        }
    )
    .then(checkResponse);
}

//03.12 tabelist rea kustutamiseks
function deleteMedDataLogRow(id) {
    return fetch(
        `${API_URL}/medinfo/${id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse);
}

// 03.12 MedData tabelis data update:
// 04.12 võiks OK olla
function updateMedDataLogCells(medData) {
    return fetch(
        `${API_URL}/medinfo/`,
        {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(medData)
        }
    )
    .then(checkResponse);
}

// kasutaja kustutamine päris halb mõte
// function deleteUser(id) {
//     return fetch(
//         `${API_URL}/medinfo/${id}`,
//         {
//             method: 'DELETE',
//             headers: {
//                 'Authorization': `Bearer ${getToken()}`
//             }
//         }
//     )
//     .then(checkResponse);
// }

function login(credentials) {
    return fetch(
        `${API_URL}/users/login`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    )
    .then(checkResponse)
    .then(session => session.json());
}

function register(user) {
    return fetch(
        `${API_URL}/users/register`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        }
    )
    .then(session => session.json());
}

//05.12 get log url by user_name:
function fetchUser(username) {
    return fetch(
        `${API_URL}/users/${username}`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse).then(user => user.json());
}

function checkResponse(response) {
    if (!response.ok) {
        clearAuthentication();
        showLoginContainer();
        closeUserModal();
        generateTopMenu();
        throw new Error(response.status);
    }
    showMainContainer();
    return response;
}

function closeUserModal() {
    $('#companyModal').modal('hide');
}


//05.12 userPhoto load:
function uploadFile(file) {
    let formData = new FormData();
    formData.append("file", file);

    return fetch(
            `${API_URL}/files/upload`, 
            {
                method: 'POST',
                body: formData
            }
    )
    .then(response => response.json());
}
  
  


