// Controller functions here...
// siin kogu äriloogika

function saveMedData() {
    let medData = getMedDataFromModal();
    if (medData) {
        postMedData(medData)
            .then(() => {
                clearInputData();
                clearMedData();

            });
    }
}


function loginUser() {
    let credentials = getCredentialsFromLoginContainer();
    if (validateCredentials(credentials)) {
        clearLoginData();
        login(credentials).then(session => {
            storeAuthentication(session);
            generateTopMenu();
        })
    }
}

function openSignUpModal() {
    $('#companyModal').modal({ backdrop: 'static', keyboard: false });
}

async function addUser() {
    let user = getUserFromModal();
    if (validateUserModal(user)) {
        let uploadResponse = await saveFile(event); //juurde
        user.logo = uploadResponse.url;
        await register(user);
        displaySuccessMessage("Success! You have been signed up successfully!");
        clearInputData();
        closeUserModal();
    }
}

function logoutUser() {
    clearAuthentication();
    generateTopMenu();
    showLoginContainer();
}

function clearInputData() {
    document.getElementById("myForm").reset();
    document.getElementById("myMedDataForms").reset();
}

function clearLoginData() {
    document.querySelector("#username").value = "";
    document.querySelector("#password").value = "";
}
function clearMedData() {
    document.querySelector("#temp").value = "";
    document.querySelector("#solids").value = "";
    document.querySelector("#liquids").value = "";
    document.querySelector("#excrement").value = "";
    document.querySelector("#urine").value = "";
    document.querySelector("#vomit").value = "";
    document.querySelector("#comments").value = "";
}


function displaySuccessMessage(messageText) {
    document.querySelector('#successMessageBox').style.display = "block";
    document.querySelector('#successMessageBox span').innerText = messageText;
    setInterval(() => {
        hideSuccessMessage();
    }, 2000);

}

function hideSuccessMessage() {
    document.querySelector('#successMessageBox span').innerText = "";
    document.querySelector('#successMessageBox').style.display = "none";
}


//03.12.2019 meie logivaate GET ja seotud funktsioonid (handleLogViewOpening, cleanLogViewFields)
async function getLogView() {
    let result = await fetch(`${API_URL}/medinfo/username/${username}`);
    let logData = await result.json();
    return logData;
}


async function loadLogTable() {
    let logData = await fetchLogView();
    displayLogTable(logData);
}

function displayLogTable(logData) {
    j = 1;
    let logHtml = "";
    for (let i = logData.length - 1; i >= 0; i--) {
        logHtml = logHtml + `
            <tr>
                <th scope="row">${j}</th>
                <td><input class="form-control" type="datetime" value="${toEstonianDateTimeString(logData[i].date)}" id="date-${logData[i].id}"></td>
                <td><input class="form-control" type="number" value="${logData[i].temp}" id="temp-${logData[i].id}"></td>
                <td><input class="form-control" type="text" value="${logData[i].solids}" id="solids-${logData[i].id}"></td>
                <td><input class="form-control" type="text" value="${logData[i].liquids}" id="liquids-${logData[i].id}"></td>
                <td><input class="form-control" type="checkbox" ${setChecked(logData[i].excrement)} id="excrement-${logData[i].id}"></td>
                <td><input class="form-control" type="checkbox" ${setChecked(logData[i].urine)} id="urine-${logData[i].id}"></td>
                <td><input class="form-control" type="checkbox" ${setChecked(logData[i].vomit)} id="vomit-${logData[i].id}"></td>
                <td><input class="form-control" type="text" value="${logData[i].comments}" id="comments-${logData[i].id}"></td>
                <td>
                <button class="btn1" onclick="handleMedDataUpdate(${logData[i].id})"><i class="fa fa-pencil-alt"></i> Update</button>
                <button class="btn2" onclick="handleMedDataDelete(${logData[i].id})"><i class="fa fa-trash-alt"></i> Delete</button></td>
            </tr>
        `
        j++;
    }

    document.querySelector("#myMedDataLogsTableBody").innerHTML = logHtml;
}

//04.12 Log view kuvamine
async function loadLogView() {
    let logData = await fetchLogView();
    displayLogView(logData);
}

function displayLogView(logData) {
    j = 1;
    let logHtml = "";
    for (let i = logData.length - 1; i >= 0; i--) {
        logHtml = logHtml + `
            <tr>
                <th scope="row">${j}</th>
                <td>${toEstonianDateTimeString(logData[i].date)}</td>
                <td>${logData[i].temp}</td>
                <td>${logData[i].solids}</td>
                <td>${logData[i].liquids}</td>
                <td>${logData[i].excrement}</td>
                <td>${logData[i].urine}</td>
                <td>${logData[i].vomit}</td>
                <td>${logData[i].comments}</td>
            </tr>
        `
        j++;
    }

    document.querySelector("#myMedDataViewTableBody").innerHTML = logHtml;
}


function toEstonianDateTimeString(isoDateStr) {
    let isoDate = new Date(isoDateStr);
    return isoDate.toLocaleString("et");
}

function toJsDate(estDateStr) {
    if (estDateStr) {
        let dateParts = estDateStr.replace(/\.|:/g, " ").split(" ");
        let myDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0], dateParts[3], dateParts[4], dateParts[5]);
        myDate.setMinutes(myDate.getMinutes() + (myDate.getTimezoneOffset() * -1));
        return myDate;
    }
    return "";
}

//05.12 logis true/false väärtuse muutmiseks enne text Booleaniks, backendist-->frontendi
function setChecked(string) {
    if (string == "true") {
        return "checked";
    } else {
        return "";
    }
}

function handleMedDataDelete(medDataId) {
    if (confirm('Are you sure you want to delete this row?')) {
        deleteMedDataLogRow(medDataId).then(loadLogTable);
    }
}

//03.12 handleMedDataUpdate
async function handleMedDataUpdate(medDataId) {
    if (confirm('Do you want to make these changes?')) {
        let updateData = getMedDataFromModalUpdate(medDataId);
        await updateMedDataLogCells(updateData);
    }
    loadLogTable();
}

//03.12.2019 lisaks date väärtus, mida saab update'i puhul muuta
function getMedDataFromModalUpdate(medDataId) {
    return {
        "id": medDataId,
        "date": toJsDate(document.querySelector('#date-' + medDataId).value),
        "temp": document.querySelector('#temp-' + medDataId).value,
        "solids": document.querySelector('#solids-' + medDataId).value,
        "liquids": document.querySelector('#liquids-' + medDataId).value,
        "excrement": document.querySelector('#excrement-' + medDataId).checked,
        "urine": document.querySelector('#urine-' + medDataId).checked,
        "vomit": document.querySelector('#vomit-' + medDataId).checked,
        "comments": document.querySelector('#comments-' + medDataId).value
    };
}

//05.12 userPhoto load
function saveFile(event) {
    event.preventDefault();
    let fileInput = getFileInput();
    return uploadFile(fileInput.files[0]);
}

