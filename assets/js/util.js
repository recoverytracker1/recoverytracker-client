
// Validation functions

function validateUserModal(user) {
    if (isEmpty(user.username)) {
        showError("Username is required!")
        return false;
    }
    if (isEmpty(user.password)) {
        showError("Password is required!")
        return false;
    }
    if (isEmpty(user.personal_code)) {
        showError("Patient personal identification number is required!")
        return false;
    }
    if (isEmpty(user.first_name)) {
        showError("User first name is required!")
        return false;
    }
    if (isEmpty(user.last_name)) {
        showError("User last name is required!")
        return false;
    }
    return true;
}

function validateCredentials(credentials) {
    return !isEmpty(credentials.username) && !isEmpty(credentials.password);
}

function isInteger(text) {
    if (text == null) {
        return false;
    }
    let regex = /^[\-]?\d+$/;
    return regex.test(text);
}

function isDecimal(text) {
    if (text == null) {
        return false;
    }
    let regex = /^[\-]?\d+(\.\d+)?$/;
    return regex.test(text);
}

function isEmpty(text) {
    return (!text || 0 === text.length);
}

// DOM retrieval functions

function getFileInput() {
    return document.getElementById("file");
}

//29.11.2019
function getMedDataFromModal() {
    return {
        "temp": document.getElementById("temp").value,
        "solids": document.getElementById("solids").value,
        "liquids": document.getElementById("liquids").value,
        "excrement": document.getElementById("excrement").checked,
        "urine": document.getElementById("urine").checked,
        "vomit": document.getElementById("vomit").checked,
        "comments": document.getElementById("comments").value
    };
}

//26.11.2019
//05.12 !!!!!!!!!!!!!!!!!!!!!!!! "logo": document.getElementById("logo").value, ==> paneme userModalisse
function getUserFromModal() {
    return {
        "user_role": "user",
        "username": document.getElementById("signUpUsername").value,
        "personal_code": document.getElementById("personalCode").value,
        "first_name": document.getElementById("firstName").value,
        "last_name": document.getElementById("lastName").value,
        "password": document.getElementById("signUpPassword").value
    };
}


function getCredentialsFromLoginContainer() {
    return {
        username: document.getElementById("username").value,
        password: document.getElementById("password").value
    };
}

//05.12 userPhoto load
function getFileInput() {
    return document.getElementById("file");
}